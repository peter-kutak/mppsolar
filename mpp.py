import dev
import time
import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(message)s')

#mppDevice = dev.MPP.factory('hid')
mppDevice = dev.MPP.factory('usb')
#mppDevice = dev.MPP.factory('serial')
mppDevice.open()

import sys
if len(sys.argv) > 1:
  #print('zadany argument {}'.format(sys.argv[1]))
  mppDevice.cmdline = True
  mppDevice.queryMsg(sys.argv[1].encode("utf-8"))
  mppDevice.close()
  exit()

import paho.mqtt.client as paho
import ssl
import os
import datetime
import time

def on_connect(client, userdata, flags, rc):
  if rc==0:
    logging.info("Connected successfully")
  else:
    logging.error("Connection problem code {}".format(rc))

def on_disconnect(client, userdata, rc):
  logging.debug("Disconnection code {}".format(rc))

def on_publish(client, userdata, mid):
  logging.info("Published msg number {}".format(mid))

def sendSenml(mppDevice):
  unixtime = int(time.time())
  #zarovnat na cele minuty
  unixtime = unixtime - (unixtime % 60)
  strt = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).replace(microsecond=0).isoformat()
  logging.debug('{}'.format(strt))
#  with open('msg-queue/msg'+datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).replace(microsecond=0).isoformat()+'.json') as file:
#    file.write('test')
  for f in os.listdir('/opt/mppsolar/msg-queue'):
    logging.debug('{}'.format(f))
  client = paho.Client()
  client.on_connect = on_connect
  client.on_disconnect = on_disconnect
  client.on_publish = on_publish
  client.username_pw_set("mpp", "mpp")
  #na serveri mam povolene iba tls1.2
#  client.tls_set('/usr/local/share/ca-certificates/compatriot-cacert.crt', tls_version=ssl.PROTOCOL_TLSv1_2)
  #host sa musi zhodovat s menom v certifikate
#  client.connect("compatriot.asuscomm.com", 8883)
  client.connect("192.168.1.200", 1883)
  client.loop(timeout=10.0)
  result = client.publish("myhome", '[{"bn":"mppsolar/","bt":'+'{}'.format(unixtime)+'}'\
                               ',{"n":"pv.voltage","v":'+'{:0.2f}'.format(mppDevice.pv_voltage.get())+',"u":"V"}'\
                               ',{"n":"pv.power","v":'+'{:0.2f}'.format(mppDevice.pv_power.get())+',"u":"W"}'\
                               ',{"n":"bat.voltage","v":'+'{:0.2f}'.format(mppDevice.bat_voltage.get())+',"u":"V"}'\
                               ',{"n":"bat.current","v":'+'{:0.2f}'.format(mppDevice.bat_current.get())+',"u":"A"}'\
                               ',{"n":"bat.chargeCurrent","v":'+'{:0.2f}'.format(mppDevice.bat_chargeCurrent.get())+',"u":"A"}'\
                               ',{"n":"bat.chargeCurrent.max","v":'+'{:0.2f}'.format(mppDevice.bat_chargeCurrent_max.get())+',"u":"A"}'\
                               ',{"n":"bat.dischargeCurrent","v":'+'{:0.2f}'.format(mppDevice.bat_dischargeCurrent.get())+',"u":"A"}'\
                               ',{"n":"bat.dischargeCurrent.max","v":'+'{:0.2f}'.format(mppDevice.bat_dischargeCurrent_max.get())+',"u":"A"}'\
                               ',{"n":"load.power","v":'+'{:0.2f}'.format(mppDevice.load_power.get())+',"u":"W"}'\
                               ',{"n":"temperature","v":'+'{:0.2f}'.format(mppDevice.temperature)+',"u":"Cel"}'\
                               ',{"n":"warning","vb":'+'{}'.format(str(mppDevice.isWarning).lower())+'}'\
                               ',{"n":"fault","vb":'+'{}'.format(str(mppDevice.isFault).lower())+'}'\
                               ',{"n":"charging.solar","vb":'+'{}'.format(str(mppDevice.charging_solar).lower())+'}'\
                               ',{"n":"charging.line","vb":'+'{}'.format(str(mppDevice.charging_line).lower())+'}'\
                               ',{"n":"output.voltage","v":'+'{:0.2f}'.format(mppDevice.output_voltage)+',"u":"V"}'\
                               ',{"n":"charging.float","vb":'+'{}'.format(mppDevice.charging_float)+'}'\
                          ']', qos=1)
  #wait_for_publish()
  for i in range(5):
    y = result.is_published()
    logging.debug("is published: {}".format(y))
    if (y):
      break
    client.loop(timeout=10.0)

  client.disconnect()
  client.loop(timeout=1.0)


##################################################################################################
#poslat na MyPower.cz
import time

def sendToMyPowerCz(fveid, mppDevice):
  unix = time.time()
  #napatie a prud na paneloch
  u_in = mppDevice.pv_voltage.get()
  i_in = mppDevice.pv_current.get()
  #napatie a prud na baterii
  u_bat = mppDevice.bat_voltage.get()
  i_bat = mppDevice.bat_current.get()
  #vykon panelov
  #p_pv =
  #spotreba ac
  p_ac = mppDevice.load_power.get()

  import urllib.request
  url = 'http://log.mypower.cz/?'
  #url = 'http://icompatriot.asuscomm.com/?'
  url = url + 'fveid={0}&unix={1:.0f}&u_in={2:0.2f}&i_in={3:0.2f}&u_bat={4:0.2f}&i_bat={5:0.2f}&p_ac={6:0.2f}'.format(fveid, unix, u_in, i_in, u_bat, i_bat, p_ac)
  logging.debug(url)
  #result = urllib.request.urlopen(url)
  #code = result.getcode()
  #print(code)
  ##data = result.read()


#######################################################################################################
# poslat mail s warningom
# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

def checkAndSendWarningEmail(mppDevice):
  sendMail = False
  if mppDevice.isFault:
    sendMail = True
  if mppDevice.isWarning:
    sendMail = True
  if sendMail:
    text = 'Warning: {}'.format(mppDevice.isWarning)
    text += '\nFault:   {}'.format(mppDevice.isFault)
    text += '\nmsg:     {}'.format(mppDevice.warning_msg)
    if mppDevice.isWarningLineFail:
      text += '\nLINE_FAIL - vypadok rozvodnej siete'
    text += '\n'
    text += mppDevice.warningText
    logging.info(text)
    msg = MIMEText(text)
    msg['Subject'] = 'mppsolar warning'
    msg['From'] = 'mpp@localhost'
    msg['To'] = 'root@localhost'

    # Send the message via our own SMTP server.
    s = None
    try:
      s = smtplib.SMTP('localhost')
      s.send_message(msg)
    except:
      logging.error("Failed to send E-mail")
    finally:
      if s is not None: s.quit()


######################################################################################3
import datetime
import q
import json

cc = paho.Client()
cc.connect("127.0.0.1", 1883)
cc.loop_start()
#siet mam odpojenu => nechcem posielat mail
ignoreLineFault = True
for j in range(0,1):
  mppDevice.clear()
  i = 0
  for k in range(0,1):
    ominute = datetime.datetime.now().minute
    nminute = ominute
    while ominute//10 == nminute//10:
      nminute = datetime.datetime.now().minute
      if i > 0:
        time.sleep(0.5)
      i = i+1
      mppDevice.queryMode()
      mppDevice.queryStatus()
      mppDevice.queryWarnings(ignoreLineFault)
      timestamp = 0
      #info pre monitoring a zapinanie vytazovaca
      msg = json.dumps({'timestamp':timestamp
                      , 'vbat':mppDevice.vbat
                      , 'pv_power':mppDevice.pv_power.getLast()
                      , 'pv_voltage':mppDevice.pv_voltage.getLast()
                      , 'load_power':mppDevice.load_power.getLast()
                      })
      cc.publish("mympp", msg, qos=0)
  cc.loop_stop()
  mppDevice.queryMode()
  mppDevice.queryStatus()
  mppDevice.queryWarnings(ignoreLineFault)

  import os
  if mppDevice.bat_voltage.get() is None:
    logging.error("Values missing")
    exit()
  else:
    #touch
    fname = '/tmp/mpp.wdt'
    with open(fname, 'a'):
        times = None
        os.utime(fname, times)

  sendSenml(mppDevice)
  fveid = "x66419f74fc"
  #sendToMyPowerCz(fveid, mppDevice)
  checkAndSendWarningEmail(mppDevice)

mppDevice.close()
logging.shutdown()

