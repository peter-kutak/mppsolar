import statistics

class Q:

    def __init__(self):
        self._values = []
        self._maxsize = 600

    def add(self, value):
        if value is None:
            if len(self._values) > 0:
                self._values.pop(0)
            return
        self._values.append(value)
        while len(self._values) > self._maxsize:
            self._values.pop(0)
            print("pop")

    def get(self):
        if len(self._values) == 0:
            return None
        return statistics.mean(self._values)

    def getLast(self):
        if len(self._values) <= 0:
            return None
        return self._values[-1]

    def clear(self):
        self._values.clear()

class Max:
    def __init__(self):
        self._value = None

    def add(self, value):
        if value is None:
            return
        if self._value is None:
            self._value = value
        if value > self._value:
            self._value = value

    def get(self):
        return self._value

    def clear(self):
        self._value = None

class Fifo:
    def __init__(self, maxsize=60):
        self._values = []
        self._maxsize = maxsize

    def add(self, value):
        self._values.append(value)
        self._trim()

    def getAll(self):
        return self._values

    def getSlice(self, length):
        return self._values[-length:]

    def getLast(self):
        if len(self._values) <= 0:
            return None
        return self._values[-1]

    def _trim(self):
        while len(self._values) > self._maxsize:
            self._values.pop(0)

