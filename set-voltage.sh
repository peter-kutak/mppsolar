#nastavovat voltage je mozne iba pre "vlastnu" bateriu
# typ 02 = custom
python3 mpp.py "PBT02"

#set battery cut-off voltage (under voltage)
python3 mpp.py "PSDV42.0"

#set battery re-charge voltage
python3 mpp.py "PBCV51.0"

#set battery re-discharge voltage
python3 mpp.py "PBDV52.0"

#set battery constant voltage charging voltage
python3 mpp.py "PCVV53.0"
#set battery float charging voltage
python3 mpp.py "PBFT52.0"

#moznosti nastavenia max nabijacieho prudu
#python3 mpp.py "QMCHGCR"
#nastavenie max nabijaceho prudu
#python3 mpp.py "MCHGC060"

#output source priority
#odkial sa ma tahat energia pre invertor
#01 - invertor je zapnuty iba ak je vykon na paneloch
#aktualne nastavenie je sucastou QPIRI
#python3 mpp.py "POP01"

