"""Zapina a vypina fotek podla potreby (dimming) """
import signal
from time import sleep
import json
import time
from datetime import datetime
from fotek import Fotek

class Vytazovac:
  value = 0
  bat_threshold = 54
  bat_redischarge = 50
  #maximalny vykon so zapnutym vytazovacom = vytazovac nesmie menit pretazit
  max_vykon = 4000
  #ocakavana spotreba vytazovaca = aby som vedel ci mam rezervu volneho vykonu
  vytazovac = 750
  pw_rezia = 65
  mozem_zapnut_count = 30
  dimming = False
  dimmer = 0

  def check(self, battery_voltage, load_power, charging_solar, charging_line, pv_power, charging_float, pv_voltage):
    vbat = battery_voltage
    pw_volny = pv_power - self.pw_rezia
    dimmer_volny = pw_volny/self.vytazovac * 100
    self.dimmer = int(dimmer_volny)
    if pv_voltage > 69 or vbat > 52:
      self.dimmer = self.dimmer + 10
    if pv_voltage > 66 or vbat > 50:
      self.dimmer = self.dimmer + 10
    if pv_voltage > 62:
      self.dimmer = self.dimmer + 5
    if vbat < 48:
      self.dimmer = self.dimmer - 10
    if vbat < 44:
      self.dimmer = self.dimmer - 50
    if vbat < 42:
      self.dimmer = 0
    if datetime.now().hour > 20:
      self.dimmer = 0
    if self.dimmer > 100:
      self.dimmer = 100
    if self.dimmer < 0:
      self.dimmer = 0
    #self.dimmer = 100
    #leovacka
    #if vbat >= 51.5 and pv_voltage > 70 and pv_power < 150:
    #  self.dimmer = 100
    print(self.dimmer)
    return self.dimmer


  def check1(self, battery_voltage, load_power, charging_solar, charging_line, pv_power, charging_float, pv_voltage):
    vbat = battery_voltage
    pw_volny = pv_power - self.pw_rezia
    dimmer_volny = pw_volny/self.vytazovac * 100
    #print("vbat {0} {1}".format(vbat, self.bat_threshold))
    #dimming
    if pv_voltage > 100:
      if vbat > 52: self.dimmer = self.dimmer + 30
    if pv_voltage > 95:
      if vbat > 51: self.dimmer = self.dimmer + 4
    if pv_voltage > 90:
      if vbat > 50: self.dimmer = self.dimmer + 1
    else:
      self.dimmer = self.dimmer - 1
    #if dimmer_volny + 5 < self.dimmer:
    #  self.dimmer = self.dimmer - 1
    if vbat < 49:
      self.dimmer = self.dimmer - 1
    if vbat < 47:
      self.dimmer = self.dimmer - 10
    if vbat < 44:
      self.dimmer = self.dimmer - 50
    if vbat < 42:
      self.dimmer = 0
    if datetime.now().hour > 20:
      self.dimmer = 0
    if self.dimmer > 100:
      self.dimmer = 100
    if self.dimmer < 0:
      self.dimmer = 0
    print(self.dimmer)
    return self.dimmer

value = 0
vytazovac = Vytazovac()
dmsg = None

def on_message(client, userdata, msg):
  """ Callback funkcia prijimajuca data, aku hodnotu nastavit """
  data = json.loads(msg.payload.decode('UTF-8'))
  print(data)
  global value
  battery_voltage = data['vbat']
  load_power = 0
  charging_solar = False
  charging_line = False
  pv_power = data['pv_power']
  charging_float = False
  pv_voltage = data['pv_voltage']
  load_power = data['load_power']
  value = vytazovac.check(battery_voltage, load_power, charging_solar, charging_line, pv_power, charging_float, pv_voltage)
  global dmsg
  dmsg = json.dumps({'timestamp':data['timestamp']
                      , 'vbat':battery_voltage
                      , 'pv_power':pv_power
                      , 'pv_voltage':pv_voltage
                      , 'load_power':load_power
                      , 'dimmer':value
                      })
  print(value)

runApp = True
def exit_gracefully(signum, frame):
    global runApp
    runApp = False

signal.signal(signal.SIGTERM, exit_gracefully)
signal.signal(signal.SIGINT, exit_gracefully)

import paho.mqtt.client as paho
def main():
  """ Telo programu """
  global runApp
  global value
  global dmsg
  f = Fotek()
  client = paho.Client()
  client.connect("127.0.0.1", 1883)
  client.loop_start()
  client.on_message = on_message
  client.subscribe('mympp', qos=0)
  tick = 0
  while runApp:
    if dmsg:
      print('publish', dmsg)
      client.publish('myvytazovac', dmsg, qos=0)
      dmsg=None
    sleep(0.01)
    tick = tick + 1
    if tick >= 100: tick = 0
    if tick < value:
      #on
      #print('on')
      f.setOn()
    else:
      #off
      #print('off')
      f.setOff()
  client.disconnect()
  sleep(0.1)
  client.loop_stop()

main()


