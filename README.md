# Mppsolar

Set of utilities for MPP-Solar(Axpert) solar charger + power invertor.
Software was used on RaspberryPi or OrangePi.

## mpp service
Reads status from charger and send data over MQTT in SenML(JSON) format.
mpp.py can be used to configure charger by sending commands from command line.
Communication with charger over USB and Serial port is supported (uncomment appropriate line in source).

## mpp-dimmer service
creepy service to use excess energy in water heater.

## chart.py
displays realtime data on text console. Data are received from localhost MQTT server send by mpp.py or mpp-dimmer.py

## watchdog
on successfull read from charger the /tmp/mpp.wdt file is written. This file can be used by watchdog service to monitor communication and restart device on problems (like problems on usb bus).

*/etc/watchdog.conf*
```
file      = /tmp/mpp.wdt
change    = 1500
```

