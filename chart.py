"""Program zobrazujuci aktualny stav"""
from time import sleep
import curses
from curses import wrapper
import json
from q import Fifo
import time

vbat = Fifo(maxsize=180)
vytaz = Fifo(maxsize=180)
pv_power = Fifo(maxsize=180)
pv_voltage = Fifo(maxsize=180)
load_power = Fifo(maxsize=180)
msg_debug = ''

def Y(value, minValue = 40, maxValue = 60, topY = 0, bottomY = 20):
  """Prepocitava hodnotu na polohu na obrazovke """
  res = int(((maxValue - value) / (maxValue-minValue)) * (bottomY-topY)) + topY
  if res<topY: res = topY
  if res>bottomY: res = bottomY
  return res

def dimmerCharH(value):
  rv = int(value/100*18)
  v = rv - 9
  if v > 9: v = 9
  if v < 0: v = 0
  if v == 0: return ' '
  return chr(int(9600+v))

def dimmerCharL(value):
  rv = int(value/100*18)
  v = rv
  if v > 9: v = 9
  if v < 0: v = 0
  if v == 0:
    if value >0:
      return chr(int(9600+1))
    else:
      return '.'
  return chr(int(9600+v))

def dimmerChar(value):
  if value > 80:
    return  '\u2588'
  if value > 60:
    return '\u2593'
  if value > 40:
    return '\u2592'
  if value >= 10:
    return '\u2591'
  return '_'

def Yv(value):
  """Prepocita Volty na polohu na obrazovke """
  return Y(value,40,60,0,20)

def Yw(value):
  """Prepocita Waty na polohu na obrazovke """
  return Y(value,0,1000,0,20)

def tick(win, rows, cols, vbat, vytaz, pv_power, pv_voltage, load_power, key):
  """Vykresli data na obrazovku """
  chart_width = cols-20
  chart_height = 23
  value_col = cols-12
  threshold = 42
  dimming = 48
  x = 0
  win.addstr(Yv(60), x, '60 -')
  win.addstr(Yv(40), x, '40 -')
  win.addstr(Yv(48), x, '48 -')
  x = 5
  for i in range(0, chart_width):
    x = x +1
    win.addstr(Yv(48), x, '-')
    win.addstr(Yv(dimming), x, '-', curses.color_pair(3))
    win.addstr(Yv(threshold), x, '-', curses.color_pair(2))
  for i in range(0, chart_height):
    win.addstr(i, 5, '|')
    win.addstr(i, 5+chart_width+1, '|')
  win.addstr(2, value_col, 'Vbat')
  win.addstr(3, value_col, str(vbat.getLast()))
  x = 5
  prevv = None
  for v in vbat.getSlice(chart_width):
    x = x + 1
    y = Yv(v)
    if prevv is not None and not v == prevv:
      start = Yv(prevv)
      stop = Yv(v)
      step = -1 if start > stop else 1
      stop = stop + step
      for iy in range(start, stop, step):
        win.addstr(iy, x, '*', curses.color_pair(1 if v > dimming else (3 if v> threshold else 2)))
    else:
      win.addstr(y, x, '*', curses.color_pair(1 if v > dimming else (3 if v> threshold else 2)))
    prevv = v
  win.addstr(5, value_col, 'pv.voltage')
  win.addstr(6, value_col, str(pv_voltage.getLast()))
  x = 5
  for v in pv_voltage.getSlice(chart_width):
    x = x + 1
    y = Y(v,50,70,0,20)
    win.addstr(y, x, '=')
  x = 5
  for v in vytaz.getSlice(chart_width):
    x = x + 1
    win.addstr(21, x, dimmerCharH(v[2]))
    win.addstr(22, x, dimmerCharL(v[2]))
  win.addstr(20, value_col, 'dimmer')
  v = vytaz.getLast()
  if v is not None:
    win.addstr(21, value_col, str(v[2]))
  win.addstr(12, value_col, 'pv.power')
  win.addstr(13, value_col, str(pv_power.getLast()))
  win.addstr(14, value_col, 'load.power')
  win.addstr(15, value_col, str(load_power.getLast()))
  x = 5
  for v in pv_power.getSlice(chart_width):
    x = x + 1
    y = Yw(v)
    win.addstr(y, x, '+')
  t = time.time()
  win.addstr(23, value_col, '{:02d}:{:02d}:{:02d}'.format(int(t//3600%24),int((t//60)%60), int(t%60)))
  win.addstr(23, 0, '{}'.format(key))
  global msg_debug
  win.addstr(24, 0, msg_debug)

def on_message(client, userdata, msg):
  """Callback funkcia prijimajuca data """
  global vbat
  global vytaz
  global pv_power
  global pv_voltage
  global load_power
  global msg_debug
  if msg.topic == 'mympp':
    return
    data = json.loads(msg.payload.decode('UTF-8'))
    #print(data)
    vbat.add(data['vbat'])
    vytaz.add((0,False,0))
    pv_power.add(data['pv_power'])
    pv_voltage.add(data['pv_voltage'])
  if msg.topic == 'myvytazovac':
    data = json.loads(msg.payload.decode('UTF-8'))
    vbat.add(data['vbat'])
    vytaz.add((0,False,data['dimmer']))
    pv_power.add(data['pv_power'])
    pv_voltage.add(data['pv_voltage'])
    load_power.add(data['load_power'])

import paho.mqtt.client as paho
def main(stdscr):
  """Hlavna funkia programu """
  curses.cbreak()
  curses.curs_set(False)
  curses.start_color()
  curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
  curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
  curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
  stdscr.nodelay(True)
  curses.halfdelay(10)
  # This is the necessary initial refresh
  stdscr.refresh()
  max_rows = 50
  max_cols = 200
  pad = curses.newpad(max_rows, max_cols)
  client = paho.Client()
  client.connect("127.0.0.1", 1883)
  client.loop_start()
  client.on_message = on_message
  #client.subscribe('mympp', qos=0)
  client.subscribe('myvytazovac', qos=0)
  runApp = True
  key = ''
  redraw = False
  while runApp:
    if redraw:
      pad.clear()
      redraw = False
    else:
      pad.erase()
    rows, cols = stdscr.getmaxyx()
    prows = min(rows, max_rows)
    pcols = min(cols, max_cols)
    tick(pad, prows, pcols, vbat, vytaz, pv_power, pv_voltage, load_power, key)
    #print('v'+str(vbat))
    pad.refresh(0,0, 0,0, rows-1,cols-1)
    try:
      key = pad.getkey()
    except Exception:
      #no input
      key = ''
    if key is '^R':
      redraw = True
    if key is 'q':
      runApp = False
   # sleep(0.1)
  client.disconnect()
  sleep(0.1)
  client.loop_stop()


wrapper(main)

