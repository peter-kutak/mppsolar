import OPi.GPIO as GPIO
#import RPi.GPIO as GPIO
#import time
from datetime import datetime

#FOTEKPIN = 18
FOTEKPIN = 28

class Fotek:

  def __init__(self, bat_threshold=54):
    #kniznica nema cisla pre moju i96 takze custom
    GPIO.set_custom_pin_mappings({FOTEKPIN:FOTEKPIN})
    GPIO.setmode(GPIO.CUSTOM)
    #GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(FOTEKPIN,GPIO.OUT)

  def setOn(self):
      GPIO.output(FOTEKPIN, GPIO.HIGH)
  def setOff(self):
      GPIO.output(FOTEKPIN, GPIO.LOW)

