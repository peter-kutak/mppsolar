"""Reprezentacia MPPSolar/Axpert"""
from time import sleep
import binascii
import q

class MPP:
  """Reprezentacia MPPSolar/Axpert"""
  cmdline = False

  def factory(deviceType):
    """
    Vytvori instanciu podla typu pripojenia
    serial - RS232
    usb - USB kabel (usblib)
    hid - USB kabel (/dev/rawhid) problem pri rekonekte skace z rawhid0 na rawhid1
    """
    if deviceType == "serial": return SerMPP()
    if deviceType == "usb": return UsbMPP()
    if deviceType == "hid": return HidMPP()

  factory = staticmethod(factory)

  def __init__(self):
    self.eol = b'\r'

    self.output_voltage = None
    self.pv_voltage = q.Q()
    self.pv_current = q.Q()
    self.bat_voltage = q.Q()
    self.vbat = None
    self.bat_current = q.Q()
    self.bat_chargeCurrent = q.Q()
    self.bat_chargeCurrent_max = q.Max()
    self.bat_dischargeCurrent = q.Q()
    self.bat_dischargeCurrent_max = q.Max()
    self.load_power = q.Q()
    self.temperature = None
    self.pv_power = q.Q()
    self.isWarning = None
    self.isWarningLineFail = None
    self.isFault = None
    self.warning_msg = None
    self.charging_solar = None
    self.charging_line = None
    self.charging_float = None

  def clear(self):
    self.pv_voltage.clear()
    self.pv_current.clear()
    self.bat_voltage.clear()
    self.bat_current.clear()
    self.bat_chargeCurrent.clear()
    self.bat_dischargeCurrent.clear()
    self.load_power.clear()
    self.pv_power.clear()

  def sendMsg(self, msg):
    return

  def recvMsg(self):
    line = self.recvRawMsg()
    assert isinstance(line, bytes), 'recvRawMsg must return bytes'
    lineLength = len(line)
    if self.cmdline:
        print('+++ recv {0} {1}'.format(line, lineLength))
    eolLength = len(self.eol)
    if lineLength <= eolLength:
        return None
    line = line[:-eolLength]
    #NAK obsahuje aj CRC
    if line == b'(NAKss':
        return None
    crcLength = 2
    if lineLength > crcLength:
        msg = line[:-crcLength]
        crcReceived = line[-crcLength:]
        crcReceived = int.from_bytes(crcReceived, byteorder='big', signed=False)
        crcCalculated = self.escapeCrc(binascii.crc_hqx(msg, 0))
        #crcC2 = self.escapeCrc(crc16.crc16xmodem(msg))
        #print('+++ crc {0} {1} {2}'.format(crcReceived, crcCalculated, crcC2))
        #vsetky odpovede zacinaju ( takze ho rovno odrezem
        start = msg[:1]
        msg = msg[1:]
        if crcReceived == crcCalculated and start == b'(':
            #print('+++ msg {0}'.format(msg))
            return msg
        else:
            print('neplatna CRC')
            return None

  def escapeCrc(self, crc):
    """
    modifikuje CRC
    Axpert pouziva specialne upravene CRC, aby neobsahoval riadiace znaky
    """
    result = crc
    msb = crc >> 8 & 0xff
    lsb = crc & 0xff
    if msb == 0x28 or msb == 0x0a or msb == 0x0d:
        result = result + 0x0100
    if lsb == 0x28 or lsb == 0x0a or lsb == 0x0d:
        result = result + 0x01
    return result

  def query(self):
    return

  def queryMsg(self, msg):
    attempt = 0
    #retry if communication fails due NAK or CRC
    while attempt < 3:
      if attempt > 0:
        sleep(0.1)
        print('retry attempt {0}'.format(attempt))
      attempt += 1
      #TODO dry pipe income
      self.sendMsg(msg)
      response = self.recvMsg()
      if response:
        return response
    return None

  def queryMode(self):
    code = self.queryMsg(b'QMOD')
    if code:
        modes = {
                  b'P': 'Power on'
                 ,b'S': 'Standby'
                 ,b'L': 'Line'
                 ,b'B': 'Battery'
                 ,b'F': 'Fault'
                 ,b'H': 'Power saving'
                }
        print('mode {0} {1}'.format(code, modes.get(code,'Unknown')))

  def queryStatus(self):
    msg = self.queryMsg(b'QPIGS')
    if msg:
        data = msg.split()
        gridVoltage = float(data[0])
        gridFrequency = float(data[1])
        acOutputVoltage = float(data[2])
        self.output_voltage = acOutputVoltage
        acOutputFrequency = float(data[3])
        acOutputApparentPower = float(data[4])
        acOutputActivePower = float(data[5])
        self.load_power.add(acOutputActivePower)
        outputLoadPercent = float(data[6])
        busVoltage = float(data[7])
        batteryVoltage = float(data[8])
        self.vbat = batteryVoltage
        self.bat_voltage.add(batteryVoltage)
        batteryChargingCurrent = float(data[9])
        self.bat_chargeCurrent.add(batteryChargingCurrent)
        self.bat_chargeCurrent_max.add(batteryChargingCurrent)
        batteryCapacity = float(data[10])
        inverterHeatsinkTemperature = float(data[11])
        self.temperature = inverterHeatsinkTemperature
        pvInputCurrentForBattery = float(data[12])
        self.pv_current.add(pvInputCurrentForBattery)
        pvInputVoltage = float(data[13])
        self.pv_voltage.add(pvInputVoltage)
        batteryVoltageFromSCC = float(data[14])
        batteryDischargeCurrent = float(data[15])
        self.bat_dischargeCurrent.add(batteryDischargeCurrent)
        self.bat_dischargeCurrent_max.add(batteryDischargeCurrent)
        deviceStatus = data[16]
        self.pv_power.add(float(data[19]))

        self.bat_current.add(batteryChargingCurrent - batteryDischargeCurrent)
        #print('load {0:.0f}%'.format(outputLoadPercent))
        #print('device status {0}'.format(deviceStatus))
        b2 = deviceStatus[5:6]
        b1 = deviceStatus[6:7]
        b0 = deviceStatus[7:8]
        self.charging_solar = isSet(b2) and isSet(b1)
        self.charging_line = isSet(b2) and isSet(b0)
        deviceStatus2 = data[20]
        b104 = deviceStatus2[0:1]
        self.charging_float = isSet(b104)
    else:
        self.output_voltage = None
        self.load_power.add(None)
        self.bat_voltage.add(None)
        self.vbat = None
        self.bat_current.add(None)
        self.bat_chargeCurrent.add(None)
        self.bat_chargeCurrent_max.add(None)
        self.bat_dischargeCurrent.add(None)
        self.bat_dischargeCurrent_max.add(None)
        self.pv_current.add(None)
        self.pv_voltage.add(None)
        self.temperature = None
        self.pv_power.add(None)
        self.charging_solar = None
        self.charging_line = None
        self.charging_float = None

  def queryWarnings(self, ignoreLine=False):
        msg = self.queryMsg(b'QPIWS')
        self.warning_msg = msg
        if msg:
            #print('warnings {0}'.format(msg))
            a0 = msg[0:1]
            a1 = msg[1:2]
            a2 = msg[2:3]
            a3 = msg[3:4]
            a4 = msg[4:5]
            a5 = msg[5:6]
            a6 = msg[6:7]
            a7 = msg[7:8]
            a8 = msg[8:9]
            a9 = msg[9:10]
            a10 = msg[10:11]
            a11 = msg[11:12]
            a12 = msg[12:13]
            a13 = msg[13:14]
            a14 = msg[14:15]
            a15 = msg[15:16]
            a16 = msg[16:17]
            a17 = msg[17:18]
            a18 = msg[18:19]
            a19 = msg[19:20]
            a20 = msg[20:21]
            a21 = msg[21:22]
            a22 = msg[22:23]
            a23 = msg[23:24]
            a24 = msg[24:25]
            a25 = msg[25:26]
            a26 = msg[26:27]
            a27 = msg[27:28]
            a28 = msg[28:29]
            a29 = msg[29:30]
            a30 = msg[30:31]
            a31 = msg[31:32]
            warning = isSet(a6) or isSet(a9) or isSet(a10) or isSet(a11) or isSet(a16) or isSet(a17) or isSet(a25) or isSet(a26) or isSet(a27) or isSet(a28) or isSet(a29)
            if not ignoreLine:
                warning = warning or isSet(a5)
            fault = isSet(a1) or isSet(a2) or isSet(a3) or isSet(a4) or isSet(a7) or isSet(a8) or isSet(a12) or isSet(a14) or isSet(a15) or isSet(a18) or isSet(a19) or isSet(a20) or isSet(a21) or isSet(a22) or isSet(a23) or isSet(a24)
            #print('Warning {0}'.format(warning))
            self.isWarning = warning
            self.isWarningLineFail = isSet(a5)
            #print('Fault {0}'.format(fault))
            self.isFault = fault
            self.warningText = ""
            if isSet(a0):
                self.warningText += "Reserved\n"
            if isSet(a1):
                self.warningText += "Inverter fault\n"
            if isSet(a2):
                self.warningText += "Bus Over\n"
            if isSet(a3):
                self.warningText += "Bus Under\n"
            if isSet(a4):
                self.warningText += "Bus Soft Fail\n"
            if isSet(a5):
                self.warningText += "LINE_FAIL\n"
            if isSet(a6):
                self.warningText += "OPVShort\n"
            if isSet(a7):
                self.warningText += "Inverter voltage too low\n"
            if isSet(a8):
                self.warningText += "Inverter voltage too high\n"
            if isSet(a9):
                self.warningText += "Over temperature\n"
            if isSet(a10):
                self.warningText += "Fan locked\n"
            if isSet(a11):
                self.warningText += "Battery voltage high\n"
            if isSet(a12):
                self.warningText += "Battery low alarm\n"
            if isSet(a13):
                self.warningText += "Reserved(Overcharge)\n"
            if isSet(a14):
                self.warningText += "Battery under shutdown\n"
            if isSet(a15):
                self.warningText += "Reserved((Battery derating)\n"
            if isSet(a16):
                self.warningText += "Over load\n"
            if isSet(a17):
                self.warningText += "Eeprom fault\n"
            if isSet(a18):
                self.warningText += "Inverter Over Current\n"
            if isSet(a19):
                self.warningText += "Inverter Soft Fail \n"
            if isSet(a20):
                self.warningText += "Self Test Fail\n"
            if isSet(a21):
                self.warningText += "OP DC Voltage Over\n"
            if isSet(a22):
                self.warningText += "Bat Open\n"
            if isSet(a23):
                self.warningText += "Current Sensor Fail\n"
            if isSet(a24):
                self.warningText += "Battery Short\n"
            if isSet(a25):
                self.warningText += "Power limit\n"
            if isSet(a26):
                self.warningText += "PV voltage high 1\n"
            if isSet(a27):
                self.warningText += "MPPT overload fault 1\n"
            if isSet(a28):
                self.warningText += "MPPT overload warning 1\n"
            if isSet(a29):
                self.warningText += "Battery too low to charge 1\n"
            #if isSet(a30):
            #    self.warningText += "PV voltage high 2\n"
            #if isSet(a31):
            #    self.warningText += "MPPT overload fault 2\n"
            #if isSet(a32):
            #    self.warningText += "MPPT overload warning 2\n"
            #if isSet(a33):
            #    self.warningText += "Battery too low to charge 2\n"
            #if isSet(a34):
            #    self.warningText += "PV voltage high 3\n"
            #if isSet(a35):
            #    self.warningText += "MPPT overload fault 3\n"
            #if isSet(a36):
            #    self.warningText += "MPPT overload warning 3\n"
            #if isSet(a37):
            #    self.warningText += "Battery too low to charge 3\n"


        else:
            self.isWarning = None
            self.isWarningLineFail = None
            self.isFault = None
            self.warningText = None


import os
class HidMPP(MPP):
  """komunikacia USB kabel cez /dev/rawhid """
  usb0 = None

  def open(self, deviceName='/dev/hidraw0'):
    self.usb0 = os.open(deviceName, os.O_RDWR | os.O_NONBLOCK)

  def readchar(self):
    try:
      #subor treba vycitavat velmi pomaly
      sleep(0.2)
      res = os.read(self.usb0, 8)
      while len(res) >0:
        #vycitany string konci nulou
        ##print("posledny znak ", res[-1:])
        if res[-1:] == b'\x00':
          #print("bez posledny znak ", res[:-1])
          res = res[:-1]
        else:
          break
    except Exception:
      return None
    return res

  def readline(self):
    leneol = len(self.eol)
    line = bytearray()
    while True:
        c = self.readchar() 
        #print("char ",c)
        if c:
            line += c
            if line[-leneol:] == self.eol:
                break
        else:
            break
    return bytes(line)

  def recvRawMsg(self):
    line = self.readline()
    return line

  def sendMsg(self, msg):
    cmd = msg
    crc = binascii.crc_hqx(msg, 0).to_bytes(2, byteorder='big')
    cmd = cmd + crc
    cmd = cmd + self.eol
    if len(cmd) <=8:
      while len(cmd) < 8:
        cmd = cmd + b'\0'
      os.write(self.usb0, cmd)

    else:
      cmd1 = cmd[:8]
      cmd2 = cmd[8:]
      while len(cmd2) < 8:
        cmd2 = cmd2 + b'\0'
      #print("cmd1 ",cmd1)
      #print("cmd2 ",cmd2)
      r1=os.write(self.usb0, cmd1)
      r2=os.write(self.usb0, cmd2)
      #print("r1 ",r1)
      #print("r2 ",r2)

    return

  def close(self):
    os.close(self.usb0)
    return

import usb.core, usb.util, usb.control
class UsbMPP(MPP):
  """pripojenie cez USB kabel (usblib) """

  def open(self, deviceName=None):
    vendorId = 0x0665
    productId = 0x5161
    interface = 0
    self.dev = usb.core.find(idVendor=vendorId, idProduct=productId)
    if self.dev.is_kernel_driver_active(interface):
      self.dev.detach_kernel_driver(interface)
    self.dev.set_interface_altsetting(0,0)


  def getCommand(self, msg):
    cmd = msg
    crc = binascii.crc_hqx(msg, 0).to_bytes(2, byteorder='big')
    cmd = cmd + crc
    cmd = cmd + self.eol
    while len(cmd) < 8:
        cmd = cmd + b'\0'
    return cmd

  def sendCommand(self, cmd):
    if len(cmd) <=8:
      self.dev.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd)
    else:
      cmd1 = cmd[:8]
      cmd2 = cmd[8:]
      while len(cmd2) < 8:
        cmd2 = cmd2 + b'\0'
      #print("cmd1 ",cmd1)
      #print("cmd2 ",cmd2)
      r1=self.dev.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd1)
      r2=self.dev.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd2)
      #print("r1 ",r1)
      #print("r2 ",r2)

  def sendMsg(self, msg):
    self.sendCommand(self.getCommand(msg))
    return

  def getResult(self, timeout=100):
    res = b""
    i = 0
    while self.eol not in res and i<20:
        try:
            res += bytes([i for i in self.dev.read(0x81, 8, timeout) if i != 0])
        except usb.core.USBError as e:
            if e.errno == 110:
                pass
            else:
                raise
        i += 1
    return res

  def recvRawMsg(self):
    res = self.getResult()
    return res

  def close(self):
    return

import serial
import io
class SerMPP(MPP):
  """cez RS232 kabel """

  timeout = 1

  def open(self, deviceName='/dev/serial0'):
    self.ser = serial.Serial(
            port = deviceName,
            baudrate = 2400,
            timeout = self.timeout
          )
    self.ser.isOpen()
    return

  def sendMsg(self, msg):
    self.ser.write(msg)
    crc = binascii.crc_hqx(msg, 0)
    self.ser.write((crc & 0xffff).to_bytes(2, byteorder='big'))
    self.ser.write(self.eol)
    return

  def readline(self):
    leneol = len(self.eol)
    line = bytearray()
    while True:
        c = self.ser.read(1)
        if c:
            line += c
            if line[-leneol:] == self.eol:
                break
        else:
            break
    return bytes(line)

  def recvRawMsg(self):
    line = self.readline()
    return line

  def close(self):
    self.ser.close()
    return


def isSet(char):
    return char != b'0'

def max(old, new):
    if old is None:
        return new
    if new is None:
        return old
    if new > old:
        return new
    return old


